# buran-manager

UI manager for buran 

# Docker Run

0. Prerequisits

Build image buran-dbmodule

1. Build

docker build -t buran-manager .

2. Run

docker run -d -it -p 1212:3000 -e DOWNLOAD_PATH={SPECIFY_DOWNLOAD_PATH} -e EVENT_SOCKET_URL={SPECIFY_SOCKET_UEL}-e DB_URL={SPECIFY_MONGO_URL} buran-manager

Example:

docker run -d -it -p 1212:3000 -e DOWNLOAD_PATH=http://192.168.104.199:1212 -e EVENT_SOCKET_URL=http://192.168.104.199:1111 -e DB_URL=mongodb://192.168.104.199:27017 buran-manager