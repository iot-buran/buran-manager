window.onload = function() {
    let url = new URL(window.location.href);
    let error = url.searchParams.get("err");
    if (error) {
        $(".error-general").text(errors[error])
        $(".error-general").show()
    }
	$(".signup-toggle").click(function(){
        $("#signup-form").show()
        $("#login-form").hide()
        $(".error-mandatory").hide()
        $(".error-general").hide()
        $(".error-general").text("")
    })
	$(".login-toggle").click(function(){
        $("#signup-form").hide()
        $("#login-form").show()
        $(".error-mandatory").hide()
        $(".error-general").hide()
        $(".error-general").text("")
    })
    $("form .btn-primary").click(function(event) {
        $(this).closest("form").find(".mandatory").each(function(index) {
			if (!$(this).val()) {
                event.preventDefault();
                $(".error-mandatory").show()
                return
            }
        })
    })    
}
