var renderItems = (items) => {
    $(".row:not(.header)").remove()
    if (!items || items.length == 0) {
		var row = $("<div/>", {"class": "row noitems"})
		$(".container-table100 .table").append(row)
		row.append(createCell(noTargetGroupsCreaterYetString, nameString))
        row.append(createCell("", dateCreatedString))
        row.append(createCell("", deleteString))
    } else {
        items.forEach(item => {
            var row = $("<div/>", {"class": "row", "data-targetgroupid": item._id})
            $(".container-table100 .table").append(row)
            row.append(createCell(item.name, nameString))
            row.append(createCell(item.dateCreated, dateCreatedString))
            let deleteCell = createCell("", deleteString);
            deleteCell.append($("<button/>", {"class": "delete-target-group", "data-targetgroupid": item._id, text: deleteString}))
            row.append(deleteCell)
        })
        setClickHandlers()
    }
}

var createCell = (value, type, extraClass) => {
	var classVal = "cell";
	if (extraClass) {
		classVal += " " + extraClass; 
	}
	return $("<div/>", {"class": classVal, "data-title": type, text: value});
}

var updateTargetGroups = () => {
	$.get( "api/fetch-target-groups", renderItems);
}

window.onload = function() {
	updateTargetGroups()
    setInterval(updateTargetGroups, 2000)
    $("#create-target-group-modal .btn-primary").click(function() {
        let hasError = false
        $(".mandatory").each(function(index) {
			if (!$(this).val()) {
                $(".error-mandatory").show()
                hasError = true;
                return
            }
        })
        if (hasError) {
            return
        }
        $(".error-mandatory").hide()       
        $.post("api/create-target-group", $("#create-target-group-form").serialize(), (data) => {
            updateTargetGroups()
            $("#create-target-group-modal").modal("hide")
        })
    })
}

let setClickHandlers = function() {
	$(".delete-target-group").click(function(){
        $.ajax({
            url: 'api/delete-target-group',
            type: 'DELETE',
            data: {targetGroupId: $(this).attr("data-targetgroupid")},
            success: function(result) {
                updateTargetGroups()
            }
        });
	})
}