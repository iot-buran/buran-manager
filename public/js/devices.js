var renderItems = (items) => {
	$(".row:not(.header)").remove()

	if (!items || items.length == 0) {
		var row = $("<div/>", {"class": "row noitems"})
		$(".container-table100 .table").append(row)
		row.append(createCell(noDeviceFoundString, deviceIdString))
		row.append(createCell("", typeString))
		row.append(createCell("", firmwareVersionString))
		row.append(createCell("", groupString, "group"))
		row.append(createCell("", statusString, "status"))
		row.append(createCell("", lastOnlineString, "last-online"))	
		row.append(createCell("", editString))
		row.append(createCell("", deleteString))
	} else {
		items.forEach(item => {
			var row = $("<div/>", {"class": "row", "data-deviceid": item._id})
			$(".container-table100 .table").append(row)
			row.append(createCell(item._id, deviceIdString))
			row.append(createCell(item.type, typeString))
			row.append(createCell(item.firmwareVersion, firmwareVersionString))
			row.append(createCell(item.group, groupString, "group"))
			row.append(createCell(deviceStatus(item.stat), statusString, "status"))
			row.append(createCell(lastOnlineDate(item.lastOnline), lastOnlineString, "last-online"))	
			let editCell = createCell("", editString);
			editCell.append($("<button/>", {"class": "edit-device", "data-deviceid": item._id, text: editString}))
			row.append(editCell)
			let deleteCell = createCell("", deleteString);
			deleteCell.append($("<button/>", {"class": "delete-device", "data-deviceid": item._id, text: deleteString}))
			row.append(deleteCell)		
		})
		setEditClickHandlers()
	}


}

var createCell = (value, type, extraClass) => {
	var classVal = "cell";
	if (extraClass) {
		classVal += " " + extraClass; 
	}
	return $("<div/>", {"class": classVal, "data-title": type, text: value});
}

var updateDevices = () => {
	$.get( "api/fetch-device-info", renderItems);
}


window.onload = function() {
	updateDevices()
	setInterval(updateDevices, 2000)
	
	setEditClickHandlers()
	
	$("#edit-device .btn-primary").click(function(){
		$.post( "api/update-device-group", {"deviceId": $("#edit-device").attr("data-deviceid"), "group": $("#edit-device input").val()}, function(data) {
		  updateDevices()
		  $("#edit-device").attr("data-deviceid", "")
		  $("#edit-device input").val("")
		  $("#edit-device").modal("hide")
		});		
	})
}

let setEditClickHandlers = function() {
	$(".edit-device").click(function() {
		$("#edit-device").attr("data-deviceid", $(this).attr("data-deviceid"))
		$("#edit-device input").val($(this).closest(".row").find(".group").text())
		$("#edit-device").modal()
	})
	$(".delete-device").click(function(){
        $.ajax({
            url: 'api/delete-device',
            type: 'DELETE',
            data: {deviceId: $(this).attr("data-deviceid")},
            success: function(result) {
                updateDevices()
            }
        });
	})		
}