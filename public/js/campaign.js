var renderItems = (items) => {
    $(".row:not(.header)").remove()
    if (!items || items.length == 0) {
		var row = $("<div/>", {"class": "row noitems"})
		$(".container-table100 .table").append(row)
		row.append(createCell(noCampaignsCreatedString, nameString))
        row.append(createCell("", targetGroupString))
        row.append(createCell("", deviceReachString))
        row.append(createCell("", startTimeString))
        row.append(createCell("", throttlingString))
        row.append(createCell("", statusString))
        row.append(createCell("", deleteString))
    } else {
        items.forEach(item => {
            var row = $("<div/>", {"class": "row", "data-campaignid": item._id})
            $(".container-table100 .table").append(row)
            row.append(createCell(item.name, nameString))
            row.append(createCell(item.targetGroupName, targetGroupString))
            row.append(createCell(item.devices ? item.devices.length : 0, deviceReachString))
            row.append(createCell(item.startTime, startTimeString))
            row.append(createCell(item.throttling, throttlingString))
            row.append(createCell(campaignStatus(item.status), statusString))
            let deleteCell = createCell("", deleteString);
            deleteCell.append($("<button/>", {"class": "delete-campaign", "data-campaignid": item._id, text: deleteString}))
            row.append(deleteCell)
        })
        setClickHandlers()
    }
}

var createCell = (value, type, extraClass) => {
	var classVal = "cell";
	if (extraClass) {
		classVal += " " + extraClass; 
	}
	return $("<div/>", {"class": classVal, "data-title": type, text: value});
}

var updateCampaigns = () => {
	$.get( "api/fetch-campaigns", renderItems);
}

window.onload = function() {
	updateCampaigns()
    setInterval(updateCampaigns, 2000)
    $("#create-campaign-modal .btn-primary").click(function() {
        let hasError = false
        $(".mandatory").each(function(index) {
			if (!$(this).val()) {
                $(".error-mandatory").show()
                hasError = true;
                return
            }
        })
        if (hasError) {
            return
        }
        $(".error-mandatory").hide()

        let form = $('#create-campaign-form')[0];

        let data = new FormData(form);
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "api/create-campaign",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
                updateCampaigns()
                $("#create-campaign-modal").modal("hide")
            },
            error: function (e) {
                window.alert(`Error: ${e}`)
                $("#create-campaign-modal").modal("hide")
            }
        })
    })
}

let setClickHandlers = function() {
	$(".delete-campaign").click(function(){
        $.ajax({
            url: 'api/delete-campaign',
            type: 'DELETE',
            data: {campaignId: $(this).attr("data-campaignid")},
            success: function(result) {
                updateCampaigns()
            }
        });
	})
}