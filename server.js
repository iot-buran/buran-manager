const EVENT_SOCKET_URL = process.env.EVENT_SOCKET_URL || 'http://localhost:3333'
const DOWNLOAD_PATH = process.env.DOWNLOAD_PATH || "http://localhost:3000"
const APP_PORT = 3000;

let express = require('express');
let exphbs  = require('express-handlebars');
let session = require('express-session');
let passwordHash = require('password-hash');
let i18n = require("i18n");
let fs = require('fs');

i18n.configure({
  locales:['bg', 'en'],
  directory: 'locales',
  defaultLocale: "bg"
});

const fileUpload = require('express-fileupload');
let db = require('dbmodule') 

db.connectToDB();

const dateFormat = "dd/mm/yyyy HH:MM:ss"

let faye = require('faye');
let eventSocket = new faye.Client(EVENT_SOCKET_URL);

eventSocket.subscribe('/device-connect', data => {
  db.upsertDevice(data.deviceId, 
      {user:data.user, 
       firmwareVersion: data.firmwareVersion, 
       type:data.type, 
       stat: "Online", 
       lastOnline: "now"
      })
    .catch(err => console.log(`Error while inserting device: ${err}`))
});
eventSocket.subscribe('/device-disconnect', data => {
  db.upsertDevice(data.deviceId, {stat: "Offline", lastOnline: require('dateformat')(new Date(), dateFormat)}).catch(err => console.log(`Error while inserting device: ${err}`))
});

eventSocket.subscribe('/device-updating', data => {
  db.upsertDevice(data.deviceId, {stat: "Updating...", lastOnline: "now"}).catch(err => console.log(`Error while inserting device: ${err}`))
});  
eventSocket.subscribe('/device-updatesuccess', data => {
  db.upsertDevice(data.deviceId, {stat: "Update Successful -> Rebooting...", lastOnline: "now"}).catch(err => console.log(`Error while inserting device: ${err}`))
});  
eventSocket.subscribe('/device-updatedfailed', data => {
  db.upsertDevice(data.deviceId, {stat: "Update Failed", lastOnline: "now"}).catch(err => console.log(`Error while inserting device: ${err}`))
});    
eventSocket.subscribe('/disconnect', () => { /* … */ });



let app = express();

app.use(i18n.init);
app.locals._ = i18n.__;
app.use((req, res, next) => {
  res.setLocale(i18n.getLocale());
  req.setLocale(i18n.getLocale());
  res.locals.i18n = res.__
  res.locals.notBG = () => i18n.getLocale() != "bg"
  next();
});
app.use(session({secret: "Your secret key"}));
app.use(function(req,res,next){
  res.locals.session = req.session;
  next();
});
app.use(["/devices", "/segmentation", "/rollout", "/api/*"], function(req, res, next){
  if(req.session.user){
    next();     //If session exists, proceed to page
  } else {
    console.log("Not logged in!");
    res.redirect('/login');
  }
});
app.use(express.urlencoded({extended:true}));
app.use(fileUpload())

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.get('/lang/:lang', function (req, res) {
  i18n.setLocale(req.params.lang)
  res.redirect('back');
})

app.get('/login', function (req, res) {
  res.render('login', {title:  req.__("Login"), noNav: true})
});

app.get('/logout', function (req, res) {
  req.session.user = undefined;
  res.redirect("/login")
});

app.post('/login', function (req, res) {
  db.getUserByEmail(req.body.email_input)
    .then(user => {
      if (user && passwordHash.verify(req.body.password_input, user.encodedPassword)) {
        req.session.user = user;
        res.redirect('/devices')
      } else {
        res.redirect("/login?err=wrongpass")
      }
    })
    .catch(err => {
      res.redirect("/login?err=error")
    })
});

app.post('/signup', async function (req, res) {
  db.getUserByEmail(req.body.email_input).then(user => {
    if (user) {
      res.redirect('/login?err=userexists') 
    } else {
      let user = {userGroupIdentifier: req.body.id_input
        , email: req.body.email_input
        , name: req.body.name_input
        , encodedPassword: passwordHash.generate(req.body.password_input)}
      db.insertNewUser(user)
      req.session.user = user;
      res.redirect('/devices')
    }
  }).catch(err => {
    res.redirect('/login?err=error') 
  }); 
})

app.get('/devices', function (req, res) {
    res.render('devices', {title: req.__("Devices")})
});


app.get('/', function (req, res) {
  res.redirect('/devices')
}); 

app.get('/segmentation', function (req, res) {
    res.render('segmentation', {title: req.__("Target Group Manager")});
}); 
app.get('/rollout', function (req, res) {
    db.getAllTargetGroupsForUser(req.session.user.userGroupIdentifier)
    .then(targetGroups => {
      res.render('rollout', {title: req.__("Rollout Manager"), targetGroups: targetGroups});
    })
    .catch(err => {
      console.log(`Error while fetching existing target-groups: ${err}`)
      res.send("error")
    })
});

app.post('/api/update-device-group', function (req, res) {
  db.upsertDevice(req.body.deviceId, {group: req.body.group})
    .then((data) => res.send("success"))
    .catch(err => {
      console.log(`Error while updating device group: ${err}`)
      res.send("error")
    })
});


app.post('/api/create-target-group', (req, res) => {
  let targetGroup = {name: req.body.name_input, idFilter: {val: req.body.id_input, op: req.body.id_operator}
      ,type: req.body.type_input
      ,fwFilter: {val: req.body.firmware_input, op: req.body.firmware_operator}
      ,groupFilter: {val: req.body.group_input, op: req.body.group_operator}
      ,dateCreated: require('dateformat')(new Date(), dateFormat)
      ,user: req.session.user.userGroupIdentifier}

  db.insertNewTargetGroup(targetGroup)
    .then((data) => res.send("success"))
    .catch(err => {
      console.log(`Error while inserting new target group: ${err}`)
      res.send("error")
    })  
});

let updateFileMove = file => {
  return new Promise((resolve, reject) => {
    let randomName = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

    if (!fs.existsSync("binaries")) {
      fs.mkdirSync("binaries")
    }
    file.mv(`binaries/${randomName}`, function(err) {
      if (err) {
        reject(err)
      }
      resolve(encodeURI(`${DOWNLOAD_PATH}/${randomName}`))
    });
  })
}

app.post('/api/create-campaign', (req, res) => {
  let targetGroupPromise = db.getTargetGroupById(req.body.target_group)

  let calculateDevicesPromise = targetGroupPromise.then(targetGroup => {
    return calculateDeviceIdsForTargetGroup(req.body.target_group, req.session.user.userGroupIdentifier)
  })

  let fileUploadPromise = calculateDevicesPromise.then(res => {
    return updateFileMove(req.files.update_file)
  })

  Promise.all([targetGroupPromise, calculateDevicesPromise, fileUploadPromise])
    .then(values => {
      let campaign = {name: req.body.name_input
        , targetGroup: values[0]._id
        , targetGroupName: values[0].name
        , throttling: req.body.throttling
        , newVersion: req.body.new_version
        , devices: values[1].map(id => {return {id: id, status: "notscheduled"}})
        , firmwareUrl: values[2]
        , status: "New"
        , startTime: req.body.start_time
        , dateCreated: require('dateformat')(new Date(), dateFormat)
        , user: req.session.user.userGroupIdentifier
      }
    
      return db.insertNewCampaign(campaign)    
    })
    .then((data) => {
      eventSocket.publish("/new-campaign", {id: data.insertedId.toString()})
      res.send("success")
    })
    .catch(err => {
      console.log(`Error while inserting new campaign: ${err}`)
      res.send("error")
    })  
});

let calculateDeviceIdsForTargetGroup = async (targetGroupId, userId) => {
  try {
    let targetGroup = await db.getTargetGroupById(targetGroupId)
    let conditions = {}

    let typeCond = {}
    typeCond['$eq'] = targetGroup.type;
    conditions["type"] = typeCond
        
    if (targetGroup.idFilter.val && targetGroup.idFilter.op) {
      let idCond = {}
      idCond[targetGroup.idFilter.op] = targetGroup.idFilter.val;
      conditions["_id"] = idCond
    }
    if (targetGroup.fwFilter.val && targetGroup.fwFilter.op) {
      let fwCond = {}
      fwCond[targetGroup.fwFilter.op] = targetGroup.fwFilter.val;
      conditions["firmwareVersion"] = fwCond
    }
    if (targetGroup.groupFilter.val && targetGroup.groupFilter.op) {
      let groupCond = {}
      groupCond[targetGroup.groupFilter.op] = targetGroup.groupFilter.val;
      conditions["group"] = groupCond
    }

    let devices = await db.getFilteredDevicesForUser(userId, conditions)
    if (devices) {
      return devices.map(dev => dev._id)
    } else {
      return []
    }
  } catch(err) {
    console.log(`Error while calculating devices for target group ${targetGroupId}: ${err}`)
  }

}

app.delete('/api/delete-campaign', (req, res) => {
  db.deteleCampaign(req.body.campaignId)
    .then((data) => {
      eventSocket.publish("/delete-campaign", {id: req.body.campaignId})
      res.send("success")
    })
    .catch(err => {
      console.log(`Error while deleting campaign: ${err}`)
      res.send("error")
    })    
})

app.delete('/api/delete-target-group', (req, res) => {
  db.deteleTargetGroup(req.body.targetGroupId)
    .then((data) => res.send("success"))
    .catch(err => {
      console.log(`Error while deleting target-group: ${err}`)
      res.send("error")
    })    
})
 
app.delete('/api/delete-device', (req, res) => {
  db.deteleDevice(req.body.deviceId)
    .then((data) => res.send("success"))
    .catch(err => {
      console.log(`Error while deleting device: ${err}`)
      res.send("error")
    })    
})

app.get("/api/fetch-target-groups", (req, res) => {
  db.getAllTargetGroupsForUser(req.session.user.userGroupIdentifier)
    .then(targetGroups => {
      res.send(targetGroups);
    })
    .catch(err => {
      console.log(`Error while fething target groups: ${err}`)
      res.send([]);
    })    
})

app.get("/api/fetch-campaigns", (req, res) => {
  db.getAllCampaignsForUser(req.session.user.userGroupIdentifier)
    .then(campaigns => {
      res.send(campaigns);
    })
    .catch(err => {
      console.log(`Error while fething campaigns: ${err}`)
      res.send([]);
    })    
})

app.get("/api/fetch-device-info", (req, res) => {	
  db.getAllDevicesForUser(req.session.user.userGroupIdentifier)
  .then(devices => {
    res.send(devices);
  })
  .catch(err => {
    console.log(`Error while fething devices: ${err}`)
    res.send([]);
  })  
})



app.use(express.static('public'))
app.use(express.static('binaries'))



app.listen(APP_PORT, () => console.log("Successfully started app"));


